# Rémy's README

## About Me :wave:

Hi! I'm Rémy and I am a Principal Engineer under the [Quality department](https://about.gitlab.com/handbook/engineering/quality) at [GitLab](https://about.gitlab.com/), specialized in [Engineering Productivity](https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/). I usually live in France (UTC+1/+2).

* :flag_fr: I grew up [30 kilometers south of Paris, France](https://goo.gl/maps/jBci6sVja2ZvUTHY7), lived for 4 years in [Lausanne, Switzerland](https://goo.gl/maps/23ikivAnjWroeBFm6), for 6 years in [Cannes, France](https://goo.gl/maps/2rng4cNoziXDnofv9), and I now settled [near Chambéry](https://goo.gl/maps/eEzTCZGnx7c7zvhq8), in the French Alps.
* :volleyball: Volleyball player since 2014.
* :diving: I used to free dive (and also sky dive, but that was crazy).
* :straight_ruler: I am 1m74 (5.7 feet).
* :book: I love to read about personal development, philosophy, and to challenge myself to look at things differently from what my instincts tell me.
* :drum: I used to play the drums in various rock bands. I love percussions in general.
* :school: I have a masters degree in computer science applied to company management, but really, what I love is to code. :nerd_face:
* :dog: I love animals. I currently live with on of my best friend and his dog!

## Books I loved (in no particular order)

* TBD.

## How I work

### My work week

I work 4 days a week since 2018. My weekly day off is usually the Friday, but sometimes I move it to another day or another week depending on my plans.

### Workflow

#### How do you start your working day?

* I start with a proper breakfast, that takes me at least 20 minutes.
* I make myself a coffee.
* I first check mentions & DMs in Slack, then my top-priority channels: `#master-broken`, `#review-apps-broken`, `#g_engineering_productivity`.
* I do a first pass at my emails: I first looked at the ones that explicitely mentions me and opens all the issues/MRs associated with them, and handle them one after one.
* I then check the rest of my emails. Most are notifications that I don't really care about or that I can just read very quickly without even opening the issue/MR (i.e. no reply needed).
* I regularly check my GitLab TODOs for things that I don't get notifications for or things that weren't clear out by a previous action from me (i.e. tasks that I delayed and that I need to handle at some point).
* I regularly check Slack's "Threads" and "All unreads" tabs.
* My SSOT for daily priorities is the [Engineering Productivity Board](https://gitlab.com/groups/gitlab-org/-/boards/978615?label_name[]=Engineering%20Productivity), where I order issues from most (issues I'm working on) to least actionable (issues waiting for reviews).
* I (usually) make myself another coffee.
* I start working on the most important task on the list

#### How do you work with emails? (cadence, moments in the day, ...)

I regularly check emails throughout the day. I'm a heavy multi-tasker.

#### When do you check slack? (open all the time, open just sometimes, on your phone, channels you check most often, ...)

I regularly check Slack throughout the day. I'm a heavy multi-tasker.

#### Do you rely on GitLab TODOs, emails, or another page to know what work you have on your plate?

Emails are the first place where I check for notifications since emails don't disappear. That way I'm sure I don't miss anything.

Then thoughout the day, I check GitLab TODOs for things that I still need to take action on.

I almost never have more than 25 TODOs at the same time, and usually end my week with 0.

#### How do you prepare for meetings?

I set up calendar events 30 minutes before the actual meeting to ensure I have enough time to prepare for the meeting, and to read the agenda before the meeting.

I try to add items as I think of them during the week.

#### How do you make async work happen in practice?

* One thing that can be surprising is that I am very reactive, in Slack and GitLab since I constantly check for notifications.
   * This could be seen as anti-async, but I see it more like a very-quick-async way of working, and I think people like that. To me, async doesn't mean slow or delayed, it mostly mean written.
* I write as much context as I can in my written communication.
* I favor writing in an issue than in Slack.

### Tools

#### What tools do you use to keep track of the stuff that cannot be in a GitLab issue (paper, TODO lists, apps, ...)

I don't really use a tool. I have a paper block where I write my grocery list, and a few personal-life TODOs, but for work, I rely on team issues in the [Engineering Productivity Board](https://gitlab.com/groups/gitlab-org/-/boards/978615?label_name[]=Engineering%20Productivity).

#### What are the tools installed on your mac that you cannot live without?

* Chrome.
* Visual Studio Code.
* SizeUp to quickly maximize windows on one of my two side-by-side screens (16-inch Built-in Retina Display and 27-inch LG UltraFine display).
* Spotify.
* Clocker (to quickly time in different timezones).
* Docker (I use [Rancher Desktop](https://rancherdesktop.io/)).
* Spark for my personal emails (I use GMail for the GitLab ones).
* IM: Slack for work, Whatsapp for private.

#### Do you have workflows that you are proud of and/or relying on constantly

* [macOS keyboard shortcuts for reviews](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/personal_productivity_tips.md).
* Using git exclusively from command line, with a few fundamental aliases (`git cob`, `git ap`, `git ci`, `git can`, `git pomr`, `git pof`, `git up`, `git rom`, `git pof`).
* [A well-configured terminal](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/local_setup.md).

### Web browser

#### What are important pages to bookmark?

TBD.

#### What are the browser extensions that you cannot live without?

* Conventional comments
* 1password
* Adblock
* JSON formatter
* Okta

#### What are other workflows/tips you have with your browser?

I started using Chrome for work, and having profiles separate profiles for work and personal sessions are great.

I heavily use tab groups for workstreams as I am very bad at finding pages once I close them :see_no_evil: : `EP flaky`, `EP metrics`, `EP pipes`, `EP workflow`, `EP RA`, `Local`, `Career`, `Mentoring`.

### Text editor

#### What is your text editor, and why did you choose this one?

I use Sublime Text for most of my development work. In the past I used Textmate and Atom, but I find Sublime Text to be the simplest and more robust over time.

I use VIM for commit message editing and interactive rebase.

I'm not in love with my editor and don't care about using "the one" editor (I'm looking at you, VIM lovers).

#### What are tips you feel everybody using your editor should know about? (e.g. multi-line edit in textmate/sublime/vscode)

* Cmd+E to select a word for search for, then Cmd+G to go to the next occurrence of it in the file.
* Cmd+D to select incrementally all occurrences of the currently selected word.
* I really use my editor in a very basic way, I don't do magic with my fingers!
